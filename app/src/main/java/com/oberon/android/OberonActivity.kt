package com.oberon.android

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.oberon.connector.OberonScreen

class OberonActivity : OberonScreen() {
    override fun onCreate(savedInstanceState: Bundle?) {
        launchBundle = MainActivity::class.java;
        projectId = "1ac96c34-18ff-477a-88c5-77af95b30e68";

        super.onCreate(savedInstanceState)
    }
}