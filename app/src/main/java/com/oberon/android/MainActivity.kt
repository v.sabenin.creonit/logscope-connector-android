package com.oberon.android

import android.os.Bundle
import android.os.StrictMode
import android.os.StrictMode.ThreadPolicy
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.lectra.koson.KosonType
import com.lectra.koson.obj
import com.oberon.connector.LogScope
import com.oberon.connector.integrations.okhttp.LogScopeOkHttpInterceptor
import okhttp3.Call
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import java.lang.Exception


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        //init
        val policy = ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        setContentView(R.layout.activity_main)

        //view
        val btn = findViewById<Button>(R.id.button);
        val ntcallBtn = findViewById<Button>(R.id.button2);

        val mode = findViewById<TextView>(R.id.mode);

        mode.text = LogScope.mode.name;

        val info: KosonType = obj {
            "test" to 2
            "a" to obj {

            }
        }

        //handlers
        ntcallBtn.setOnClickListener {
            val client = OkHttpClient.Builder()
                .addInterceptor(LogScopeOkHttpInterceptor()) // interceptor
                .build()

            val request: Request = Request.Builder()
                .url("https://api.ipify.org?format=json")
                .build()

           try{
               val call: Call = client.newCall(request)
               val response: Response = call.execute()
           }catch(ex: Exception) {
                LogScope.logException(ex)
           }

        }

        btn.setOnClickListener {
            LogScope.logInfo("im alive", payload = obj {
                "test" to 12
            })
        }

        super.onCreate(savedInstanceState)
    }
}