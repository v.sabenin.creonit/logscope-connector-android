/**
 * My Title
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
package io.swagger.client.models


/**
 * 
 * @param projectID 
 * @param page 
 * @param pageSize 
 * @param deviceId 
 * @param associatedAccountPhone 
 * @param associatedAccountEmail 
 */
data class DevicesFilter (

    val projectID: kotlin.String? = null,
    val page: kotlin.Int? = null,
    val pageSize: kotlin.Int? = null,
    val deviceId: kotlin.String? = null,
    val associatedAccountPhone: kotlin.String? = null,
    val associatedAccountEmail: kotlin.String? = null
) {
}