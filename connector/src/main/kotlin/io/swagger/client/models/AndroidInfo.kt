/**
 * My Title
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
package io.swagger.client.models

import io.swagger.client.models.AndroidVersion
import io.swagger.client.models.DisplayMetrics

/**
 * 
 * @param id 
 * @param host 
 * @param manifacturer 
 * @param androidVersion 
 * @param board 
 * @param bootLoader 
 * @param brand 
 * @param device 
 * @param display 
 * @param fingerprint 
 * @param hardware 
 * @param isPhysical 
 * @param model 
 * @param product 
 * @param serialNumber 
 * @param supported32BitAbis 
 * @param supported64BitAbis 
 * @param supportedAbis 
 * @param systemFeatures 
 * @param type 
 */
data class AndroidInfo (

    val id: kotlin.String,
    val host: kotlin.String,
    val manifacturer: kotlin.String,
    val androidVersion: AndroidVersion,
    val board: kotlin.String,
    val bootLoader: kotlin.String,
    val brand: kotlin.String,
    val device: kotlin.String,
    val display: DisplayMetrics,
    val fingerprint: kotlin.String,
    val hardware: kotlin.String,
    val isPhysical: kotlin.Boolean,
    val model: kotlin.String,
    val product: kotlin.String,
    val serialNumber: kotlin.String,
    val supported32BitAbis: kotlin.Array<kotlin.String>,
    val supported64BitAbis: kotlin.Array<kotlin.String>,
    val supportedAbis: kotlin.Array<kotlin.String>,
    val systemFeatures: kotlin.Array<kotlin.String>,
    val type: kotlin.String
) {
}