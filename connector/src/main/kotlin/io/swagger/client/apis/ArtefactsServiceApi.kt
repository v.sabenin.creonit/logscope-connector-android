/**
 * My Title
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
package io.swagger.client.apis

import io.swagger.client.models.ArtefactsListRequest
import io.swagger.client.models.PageListOfArtefactDTO
import io.swagger.client.models.ProblemDetails

import io.swagger.client.infrastructure.*

class ArtefactsServiceApi(basePath: kotlin.String = "http://localhost:8080") : ApiClient(basePath) {

    /**
     * 
     * 
     * @param body  
     * @return PageListOfArtefactDTO
     */
    @Suppress("UNCHECKED_CAST")
    fun artefactsServiceArtefacts(body: ArtefactsListRequest): PageListOfArtefactDTO {
        val localVariableBody: kotlin.Any? = body
        val localVariableConfig = RequestConfig(
                RequestMethod.POST,
                "/api/artefacts"
        )
        val response = request<PageListOfArtefactDTO>(
                localVariableConfig, localVariableBody
        )

        return when (response.responseType) {
            ResponseType.Success -> (response as Success<*>).data as PageListOfArtefactDTO
            ResponseType.Informational -> TODO()
            ResponseType.Redirection -> TODO()
            ResponseType.ClientError -> throw ClientException((response as ClientError<*>).body as? String ?: "Client error")
            ResponseType.ServerError -> throw ServerException((response as ServerError<*>).message ?: "Server error")
        }
    }
    /**
     * 
     * 
     * @param contentType  (optional)
     * @param contentDisposition  (optional)
     * @param headers  (optional)
     * @param length  (optional)
     * @param name  (optional)
     * @param fileName  (optional)
     * @param projectId  (optional)
     * @param ver  (optional)
     * @param environment  (optional)
     * @param flavor  (optional)
     * @param platform  (optional)
     * @param build  (optional)
     * @param artefactExt  (optional)
     * @return kotlin.String
     */
    @Suppress("UNCHECKED_CAST")
    fun artefactsServiceUpload(contentType: kotlin.String? = null, contentDisposition: kotlin.String? = null, headers: kotlin.Array<kotlin.Any>? = null, length: kotlin.Long? = null, name: kotlin.String? = null, fileName: kotlin.String? = null, projectId: kotlin.String? = null, ver: kotlin.String? = null, environment: kotlin.String? = null, flavor: kotlin.String? = null, platform: kotlin.String? = null, build: kotlin.Int? = null, artefactExt: kotlin.String? = null): kotlin.String {
        val localVariableBody: kotlin.Any? = mapOf("ContentType" to "$contentType", "ContentDisposition" to "$contentDisposition", "Headers" to "$headers", "Length" to "$length", "Name" to "$name", "FileName" to "$fileName")
        val localVariableQuery: MultiValueMap = mutableMapOf<kotlin.String, kotlin.collections.List<kotlin.String>>().apply {
            if (projectId != null) {
                put("projectId", listOf(projectId.toString()))
            }
            if (ver != null) {
                put("ver", listOf(ver.toString()))
            }
            if (environment != null) {
                put("environment", listOf(environment.toString()))
            }
            if (flavor != null) {
                put("flavor", listOf(flavor.toString()))
            }
            if (platform != null) {
                put("platform", listOf(platform.toString()))
            }
            if (build != null) {
                put("build", listOf(build.toString()))
            }
            if (artefactExt != null) {
                put("artefactExt", listOf(artefactExt.toString()))
            }
        }
        val localVariableHeaders: MutableMap<String, String> = mutableMapOf("Content-Type" to "multipart/form-data")
        localVariableHeaders["Accept"] = "application/json"
        val localVariableConfig = RequestConfig(
                RequestMethod.POST,
                "/api/artefacts/create", query = localVariableQuery, headers = localVariableHeaders
        )
        val response = request<kotlin.String>(
                localVariableConfig, localVariableBody
        )

        return when (response.responseType) {
            ResponseType.Success -> (response as Success<*>).data as kotlin.String
            ResponseType.Informational -> TODO()
            ResponseType.Redirection -> TODO()
            ResponseType.ClientError -> throw ClientException((response as ClientError<*>).body as? String ?: "Client error")
            ResponseType.ServerError -> throw ServerException((response as ServerError<*>).message ?: "Server error")
        }
    }
}
