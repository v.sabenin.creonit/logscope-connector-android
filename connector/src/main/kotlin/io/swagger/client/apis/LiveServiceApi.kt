/**
 * My Title
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
package io.swagger.client.apis

import io.swagger.client.models.DeviceInfo
import io.swagger.client.models.LiveDevicesListRequest

import io.swagger.client.infrastructure.*

class LiveServiceApi(basePath: kotlin.String = "http://localhost:8080") : ApiClient(basePath) {

    /**
     * 
     * 
     * @param body  
     * @return kotlin.Array<DeviceInfo>
     */
    @Suppress("UNCHECKED_CAST")
    fun liveServiceGetLiveDevices(body: LiveDevicesListRequest): kotlin.Array<DeviceInfo> {
        val localVariableBody: kotlin.Any? = body
        val localVariableConfig = RequestConfig(
                RequestMethod.POST,
                "/api/live/list"
        )
        val response = request<kotlin.Array<DeviceInfo>>(
                localVariableConfig, localVariableBody
        )

        return when (response.responseType) {
            ResponseType.Success -> (response as Success<*>).data as kotlin.Array<DeviceInfo>
            ResponseType.Informational -> TODO()
            ResponseType.Redirection -> TODO()
            ResponseType.ClientError -> throw ClientException((response as ClientError<*>).body as? String ?: "Client error")
            ResponseType.ServerError -> throw ServerException((response as ServerError<*>).message ?: "Server error")
        }
    }
    /**
     * 
     * 
     * @param deviceID  (optional)
     * @param projectID  (optional)
     * @return void
     */
    fun liveServiceSubscribeOnDevice(deviceID: kotlin.String? = null, projectID: kotlin.String? = null): Unit {
        val localVariableQuery: MultiValueMap = mutableMapOf<kotlin.String, kotlin.collections.List<kotlin.String>>().apply {
            if (deviceID != null) {
                put("deviceID", listOf(deviceID.toString()))
            }
            if (projectID != null) {
                put("projectID", listOf(projectID.toString()))
            }
        }
        val localVariableConfig = RequestConfig(
                RequestMethod.GET,
                "/api/live/events", query = localVariableQuery
        )
        val response = request<Any?>(
                localVariableConfig
        )

        return when (response.responseType) {
            ResponseType.Success -> Unit
            ResponseType.Informational -> TODO()
            ResponseType.Redirection -> TODO()
            ResponseType.ClientError -> throw ClientException((response as ClientError<*>).body as? String ?: "Client error")
            ResponseType.ServerError -> throw ServerException((response as ServerError<*>).message ?: "Server error")
        }
    }
}
