package com.oberon.connector

import android.content.Context
import android.os.Build
import android.provider.Settings.Secure
import android.util.Log
import androidx.annotation.RequiresApi
import com.google.gson.Gson
import com.lectra.koson.KosonType
import com.lectra.koson.arr
import com.lectra.koson.obj
import io.swagger.client.apis.ConsumerApi
import io.swagger.client.models.AndroidInfo
import io.swagger.client.models.AndroidVersion
import io.swagger.client.models.BundleInfo
import io.swagger.client.models.DisplayMetrics
import io.swagger.client.models.EventsBatch
import io.swagger.client.models.Identification
import io.swagger.client.models.RegisteredEvent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import java.io.BufferedReader
import java.io.File
import java.io.FileReader
import java.io.FileWriter
import java.security.MessageDigest
import java.util.UUID


enum class EnvironmentMode{
    Production,
    Development
}

object LogScope {
	val scope: CoroutineScope = CoroutineScope(Dispatchers.Default)

	var a: Int = 2;

	var deviceId: UUID = UUID.randomUUID()
	lateinit var projectId: String;

	val channel = Channel<RegisteredEvent>()
	var eventsCache = mutableSetOf<RegisteredEvent>();
	val gson = Gson()
	val mutex = Mutex()

	lateinit var mode: EnvironmentMode;

	fun addLog(log: RegisteredEvent) {
		Log.i("LogScope", "Получен лог");
		runBlocking {
			channel.send(log);
			eventsCache.add(log);
		}
	}

	@OptIn(ExperimentalStdlibApi::class)
	fun String.md5(): UUID {
		val md = MessageDigest.getInstance("MD5")
		val digest = md.digest(this.toByteArray())
		return UUID.nameUUIDFromBytes(digest)
	}

	fun logInfo(identification: String, payload: KosonType? = null) {
		addLog(
			RegisteredEvent(
				id = UUID.randomUUID().toString(),
				timestamp = System.currentTimeMillis(),
				identification = identification,
				kind = "Глобальный",
				scope = "Информация",
				severity = "Информация",
				payload = payload?.toString() ?: "{}",
				starred = false,
			)
		)
	}

	fun logException(
		ex: Exception
	) {
		logException(ex.message, ex.stackTrace)
	}

	fun logException(
		identification: String?,
		stacktrack: Array<StackTraceElement> = arrayOf()
	) {
		addLog(
			RegisteredEvent(
				id = UUID.randomUUID().toString(),
				timestamp = System.currentTimeMillis(),
				identification = identification ?: "Без сообщения",
				kind = "Глобальный",
				scope = "Информация",
				severity = "Ошибка",
				payload = obj {
					"stacktrace" to arr[stacktrack.map {
						obj {
							"filename" to it.fileName
							"classname" to it.className
							"methodname" to it.methodName
							"line" to it.lineNumber
						}
					}]
				}.toString(),
				starred = false,
			)
		)
	}

	public fun rawLog(
		identification: String,
		scope: String,
		kind: String,
		severity: String,
		payload: KosonType?
	) {
		addLog(
			RegisteredEvent(
				id = UUID.randomUUID().toString(),
				timestamp = System.currentTimeMillis(),
				identification = identification,
				kind = kind,
				scope = scope,
				severity = severity,
				payload = payload?.toString() ?: "{}",
				starred = false,
			)
		)
	}

	@OptIn(FlowPreview::class)
	@RequiresApi(Build.VERSION_CODES.M)
	fun start(context: Context, projectId: String) {
		val path = context.filesDir

		LogScope.projectId = projectId;
		catchErrors()
		val android_id = Secure.getString(
			context.contentResolver,
			Secure.ANDROID_ID
		)
		deviceId = android_id.md5();
		scope.launch(Dispatchers.IO) {
			channel.consumeAsFlow()
				.debounce(500)
				.collectLatest {
					val events = eventsCache.toTypedArray();
					eventsCache.clear();

					val bundle = BundleInfo(version = "", build = "")

					val batch = EventsBatch(
						isLive = true,
						events = events,
						projectID = projectId,
						bundle = bundle,
						os = 1,
						identification = Identification(code = deviceId.toString()),
						androidInfo = AndroidInfo(
							id = Build.ID,
							host = Build.HOST,
							manifacturer = Build.MANUFACTURER,
							board = Build.BOARD,
							bootLoader = Build.BOARD,
							brand = Build.BRAND,
							device = Build.DEVICE,
							display = DisplayMetrics(
								widthPx = 500,
								heightPx = 500,
								xDpi = 1,
								yDpi = 1
							),
							fingerprint = Build.FINGERPRINT,
							hardware = Build.HARDWARE,
							isPhysical = !Build.MODEL.contains("google_sdk") &&
								!Build.MODEL.contains("Emulator"),
							model = Build.MODEL,
							product = Build.PRODUCT,
							serialNumber = "UNKNOWN",
							supported32BitAbis = Build.SUPPORTED_32_BIT_ABIS,
							supported64BitAbis = Build.SUPPORTED_64_BIT_ABIS,
							supportedAbis = Build.SUPPORTED_ABIS,
							systemFeatures = arrayOf(""),
							type = Build.TYPE,
							androidVersion = AndroidVersion(
								baseOS = Build.VERSION.BASE_OS,
								previewSdkInt = Build.VERSION.PREVIEW_SDK_INT,
								securityPatch = Build.VERSION.SECURITY_PATCH,
								codeName = Build.VERSION.CODENAME,
								incremental = Build.VERSION.INCREMENTAL,
								release = Build.VERSION.RELEASE,
								sdkInt = Build.VERSION.SDK_INT,
							)
						)
					)
					var logCount = 1
					path.listFiles()?.sortedBy { it.name }?.let { files ->
						if (files.isNotEmpty()) {
							logCount = files.last().name.toInt() + 1
							val file = File(path, "$logCount")
							val batchJson = gson.toJson(batch)
							writeToFile(file, batchJson)
							Log.e("LogScope", "Пакет сохранен")
						} else {
							val file = File(path, "$logCount")
							val batchJson = gson.toJson(batch)
							logCount = 1
							writeToFile(file, batchJson)
							Log.e("LogScope", "Пакет сохранен")
						}
					}
				}
		}
		scope.launch {
			while (true) {
				try {
					path.listFiles()?.sortedBy { it.name }?.let { files ->
						if (files.isNotEmpty()) {
							mutex.withLock {
								val client = ConsumerApi("https://logscope.ru:443")
								val file = files.first()
								val dataInFile = file.readText()
								val eventBatch = gson.fromJson(dataInFile, EventsBatch::class.java)
								client.consumerConsumeEvents(eventBatch)
								file.delete()
								Log.e("LogScope", "Пакет отправлен")
							}
						}
					}
				} catch (ex: Exception) {
					Log.e("LogScope", "Ошибка отправки пакета $ex")
				}

				delay(1000L)
			}
		}
	}

	fun catchErrors() {
		// val androidID = Secure.getString((this as Context).contentResolver, Secure.ANDROID_ID)

		// Log.d("Connector", androidID)
		Thread.setDefaultUncaughtExceptionHandler { paramThread, paramThrowable ->
			addLog(
				RegisteredEvent(
					id = UUID.randomUUID().toString(),
					timestamp = System.currentTimeMillis(),
					paramThrowable.message ?: "",
					"Глобальный",
					"Краши",
					severity = "Ошибка",
					payload = "{}",
					starred = false,
				)
			)
		}
	}

	suspend fun writeToFile(file: File, data: String) = mutex.withLock {
		val fileWriter = FileWriter(file, true)
		fileWriter.append(data)
		fileWriter.append("\n")
		fileWriter.close()
	}

	private fun readFromFile(file: File): String
	{
		val text = StringBuilder()
		val bufferReader = BufferedReader(FileReader(file))

		text.append(bufferReader.readText())

		bufferReader.close()
		return text.toString()
	}
}
