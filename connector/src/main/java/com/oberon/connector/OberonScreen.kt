package com.oberon.connector

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity


open class OberonScreen() : AppCompatActivity() {
    public lateinit var launchBundle: Class<*>;
    public lateinit var projectId: String;

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        supportActionBar?.hide();

        Log.i("LogScope", "Запуск системы");
        LogScope.start(applicationContext, projectId);

        setContentView(R.layout.activity_oberon_screen)

        var idLabel = findViewById<TextView>(R.id.codeLabel)

        idLabel.text = LogScope.deviceId.toString();

        var cts = this as Context;

        var devBtn = findViewById<Button>(R.id.dev);
        var prodBtn = findViewById<Button>(R.id.prod);

        devBtn.setOnClickListener {
            finish()
            val intent = Intent(cts, launchBundle)
            LogScope.mode = EnvironmentMode.Development;
            startActivity(intent)
        }

        prodBtn.setOnClickListener {
            finish()
            val intent = Intent(cts, launchBundle)
            LogScope.mode = EnvironmentMode.Production;
            startActivity(intent)
        }
    }
}