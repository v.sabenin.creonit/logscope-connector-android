package com.oberon.connector.integrations.okhttp

import android.util.Log
import com.lectra.koson.KosonType
import com.lectra.koson.obj
import com.oberon.connector.LogScope
import okhttp3.Interceptor
import okhttp3.Response

class LogScopeOkHttpInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()

        var pld: KosonType

        Log.i("LogScope", "Запрос перехвачен");

        try {
            var result = chain.proceed(request);

            pld = obj {
                "https" to request.isHttps
                "url" to request.url.toString()
                "method" to request.method
                "status" to result.code
                "isRedirect" to result.isRedirect
                "requestHeaders" to obj {
                    for (header in request.headers)
                        header.first to header.second
                }
                "responseHeaders" to obj {
                    for (header in result.headers)
                        header.first to header.second
                }
            }
            LogScope.rawLog("Сетевой запрос", "Главный клиент", "Уведомление", "Отладка", pld)
            return result;
        } catch (ex: Exception) {
            pld = obj{
                "https" to request.isHttps
                "url" to request.url.toString()
                "method" to request.method
                "failedToCall" to true
                "exception" to ex.message
            }
            LogScope.rawLog("Ошибка запроса", "Главный клиент", "Уведомление", "Отладка", pld)
            throw ex;
        }
    }
}