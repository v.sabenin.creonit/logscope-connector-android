package com.oberon.connector.integrations.timber

import com.oberon.connector.LogScope
import timber.log.Timber

class OberonTree : Timber.Tree() {
    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
        LogScope.logInfo(message);
    }
}